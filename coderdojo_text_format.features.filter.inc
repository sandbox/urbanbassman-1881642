<?php
/**
 * @file
 * coderdojo_text_format.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function coderdojo_text_format_filter_default_formats() {
  $formats = array();

  // Exported format: CoderDojo Text Content.
  $formats['coderdojo_text_content'] = array(
    'format' => 'coderdojo_text_content',
    'name' => 'CoderDojo Text Content',
    'cache' => '1',
    'status' => '1',
    'weight' => '0',
    'filters' => array(
      'filter_html' => array(
        'weight' => '-10',
        'status' => '1',
        'settings' => array(
          'allowed_html' => '<h1> <h2> <h3> <h4> <h5> <h6> <p> <table> <thead> <tbody> <tfoot> <tr> <th> <td> <a> <abbr> <acronym> <address> <b> <br> <blockquote> <cite> <code> <dl> <dt> <dd> <em> <i> <img> <li> <ol> <pre> <samp> <strong> <sub> <sup>  <ul>',
          'filter_html_help' => 1,
          'filter_html_nofollow' => 0,
        ),
      ),
      'filter_htmlcorrector' => array(
        'weight' => '10',
        'status' => '1',
        'settings' => array(),
      ),
    ),
  );

  return $formats;
}
